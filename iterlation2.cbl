       IDENTIFICATION DIVISION. 
       PROGRAM-ID. USER-DIV.
       AUTHOR. CindyCheeseCake.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  NUM1     PIC    9(5)    VALUE 0.
       01  NUM2     PIC    9(5)    VALUE 0.
       01  RESULT   PIC    9(5)V9(5) VALUE 0.
       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 001-PRINT-STAR-INLINE  THRU 001-EXIT
           PERFORM 002-PRINT-STAR-INLINE  THRU 001-EXIT
       .
       001-PRINT-STAR-INLINE.
           PERFORM 10 TIMES 
              DISPLAY "*" WITH NO ADVANCING 
           END-PERFORM
       .
       001-EXIT.
           EXIT.
       002-PRINT-STAR-INLINE.
           PERFORM 003-PRINT-ONE-STAR 10 TIMES
       .
       002-EXIT.
           EXIT.
       003-PRINT-ONE-STAR.
              DISPLAY "*" WITH NO ADVANCING 
       .