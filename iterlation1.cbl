       IDENTIFICATION DIVISION. 
       PROGRAM-ID. USER-DIV.
       AUTHOR. CindyCheeseCake.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  NUM1     PIC    9(5)    VALUE 0.
       01  NUM2     PIC    9(5)    VALUE 0.
       01  RESULT   PIC    9(5)V9(5) VALUE 0.
       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 001-USER-DIV  THRU 001-EXIT
       .
       001-USER-DIV.
           DISPLAY "Please input NUM1: " WITH NO ADVANCING
           ACCEPT  NUM1
           DISPLAY "Please input NUM2: " WITH NO ADVANCING
           ACCEPT  NUM2
           If NUM2 = 0 THEN
              DISPLAY "ERROR NUM2 IS ZERO."
              GO TO 001-EXIT
           END-IF
           COMPUTE NUM1/NUM2
           DISPLAY "RESULT IS " NUM1/NUM2
       .
       001-EXIT.
           EXIT.